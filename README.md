Web Animation for use in live streams.

The particle system used is [particle.js](https://vincentgarreau.com/particles.js/), under MIT license.


The Tech Pills logo <img src="techPillsLogo.svg" height="30" /> is owned by [Gabriele Musco](mailto:gabmus@disroot.org). All rights reserved.

The svg graphics files in this repo are released under the Creative Commons Attribution-ShareAlike 4.0 International license. For details consult the `LICENSE` file in this repository or [this link](https://creativecommons.org/licenses/by-sa/4.0/).
